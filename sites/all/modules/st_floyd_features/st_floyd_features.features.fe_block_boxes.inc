<?php
/**
 * @file
 * st_floyd_features.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function st_floyd_features_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact form';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'contact_form';
  $fe_block_boxes->body = '<!-- End Contact Details -->
<div class="container">
  <div class="col-md-8 col-md-offset-2">
		<?php
			if(!function_exists(\'contact_site_page\')) {
			  require_once drupal_get_path(\'module\', \'contact\').\'/contact.pages.inc\';
			  $output = drupal_get_form(\'contact_site_form\');
			  print drupal_render($output);
			}
		?>
	</div>
</div>';

  $export['contact_form'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact information';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'contact_info';
  $fe_block_boxes->body = '<!-- Contact Details End-->
<div id="contact-details">
	<div class="gray-bg contact-details">
		<div class="container">
			<div class="row">
				<h3 class="text-center title">Contact Details</h3>
				<div class="contact-info">
				
						<div class="col-md-3 col-sm-6 contact-info-box wow bounceInDown center animated" data-wow-delay=".2s">

							<span class="icon map-marker">
								<i aria-hidden="true" class="li_location"> </i>
							</span>
							<p class="contact-details-title">Address</p>
							<span class="texts">Inner Circular Road, Broker <br>
								Sector Rang Plaza, UK
							</span>
						</div> <!-- /.contact-info-box -->

						<div class="col-md-3 col-sm-6 contact-info-box wow bounceInDown center animated" data-wow-delay=".4s">
							<span class="icon envelope">
								<i aria-hidden="true" class="li_mail"></i>
							</span>
							<p class="contact-details-title">Email</p>
							<span class="texts"><a href="#">hello@topclass.com</a> <br>
								<a href="#">info@topclass.com</a>
							</span>
						</div><!--  /.contact-info-box -->

						<div class="col-md-3 col-sm-6 contact-info-box wow bounceInDown center animated" data-wow-delay=".6s">
							<span class="icon phone">
								<i aria-hidden="true" class="li_phone"></i>
							</span>
							<p class="contact-details-title">Phone</p>
							<span class="texts">+61 (001) 2345 67890 <br>
								+61 (001) 2345 67890
							</span>
						</div><!-- /.contact-info-box -->

						<div class="col-md-3 col-sm-6 contact-info-box wow bounceInDown center animated" data-wow-delay=".8s">
							<span class="icon skype"><i class="fa fa-skype"></i></span>
							<p class="contact-details-title">Skype</p>
							<span class="texts"><a href="#">support.topclass@skype.com</a> <br>
								<a href="#">help.topclass@skype.com</a>
							</span>
						</div><!-- /.contact-info-box -->
				</div><!-- /.contact-info -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.gray-bg  /.contact-details -->
</div><!-- /#contact-details -->
<!-- Contact Details End-->';

  $export['contact_info'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact map';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'contact_map';
  $fe_block_boxes->body = '<div class="container">
  <div class="title col-md-8 col-sm-10 col-xs-12">
    <h1>Contact <strong>us</strong></h1>
    <hr>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua</p>
  </div>
</div>

<!-- Map -->
<div id="map"></div>
<!-- Contact Details -->
<div class="contact-details">
  <div class="container">
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="detail">
        <i class="fa fa-map-marker"></i>
        <span>New York, NY</span>
      </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="detail">
        <i class="fa fa-phone"></i>
        <span>(987) 654-3210</span>
      </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="detail last">
        <i class="fa fa-envelope"></i>
        <span>info@floyd.com</span>
      </div>
    </div>
  </div>
</div>';

  $export['contact_map'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'footer';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'footer';
  $fe_block_boxes->body = '<div class="container">
  <!-- Social Links -->
  <ul class="social-list">
    <li>
      <a href="https://www.facebook.com/" target="_blank">
        <span class="social-icon">
          <i class="fa fa-facebook"></i>
        </span>
      </a>
    </li>
    <li>
      <a href="https://twitter.com/" target="_blank">
        <span class="social-icon">
          <i class="fa fa-twitter"></i>
        </span>
      </a>
    </li>
    <li>
      <a href="https://plus.google.com/" target="_blank">
        <span class="social-icon">
          <i class="fa fa-google-plus"></i>
        </span>
      </a>
    </li>
  </ul>
  <!-- End Social Links -->

  <!-- Copyright -->
  <p class="center grey">2013 &copy; Floyd. All rights reserved.</p>
  <!-- End Copyright -->
</div>   ';

  $export['footer'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Number';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'number_section';
  $fe_block_boxes->body = '<div class="parallax-overlay">
  <div class="container">
    <h4 class="center up">Our numbers</h4>
    <div class="col-md-3 col-sm-3 col-xs-6">          
      <span class="counter wow fadeInDown animated animated" data-wow-delay="0s">
        <i class="fa fa-group fa-4x"></i>
        <span class="value">150</span>
        <small>clients</small>
      </span>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6">          
      <span class="counter wow fadeInDown animated animated" data-wow-delay="0.5s">
        <i class="fa fa-trophy fa-4x"></i>
        <span class="value">450</span>
        <small>projects</small>
      </span>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6">          
      <span class="counter wow fadeInDown animated animated" data-wow-delay="1s">
        <i class="fa fa-coffee fa-4x"></i>
        <span class="value">380</span>
        <small>coffee cups</small>
      </span>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6">          
      <span class="counter wow fadeInDown animated animated" data-wow-delay="1.5s">
        <i class="fa fa-code fa-4x"></i>
        <span class="value">888150</span>
        <small>lines of code</small>
      </span>
    </div>
  </div>
</div>';

  $export['number_section'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Our Skills';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'our_skill';
  $fe_block_boxes->body = '<div class="gray-bg top-skills">
	<h3 class="text-center top-skills-title">Our top Skills</h3>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="progress thin wow fadeInUp animated" data-wow-delay=".1s">
					<div class="progress-bar default-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" data-progress-value="95%" aria-valuemax="100">
						<span class="progress-text">Adobe Photoshop</span> 
						<span class="progress-percent"> 95% </span>
					</div>
				</div>
				<div class="progress thin wow fadeInUp animated" data-wow-delay=".2s">
					<div class="progress-bar light-yellow-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" data-progress-value="70%" aria-valuemax="100">
						<span class="progress-text">Adobe Illustrator </span> 
						<span class="progress-percent"> 70% </span>
					</div>
				</div>
				<div class="progress thin wow fadeInUp animated" data-wow-delay=".3s">
					<div class="progress-bar light-red-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" data-progress-value="60%" aria-valuemax="100">
						<span class="progress-text">Adobe InDesign</span> 
						<span class="progress-percent"> 60% </span>
					</div>
				</div>
			</div><!-- /.col-md-6 -->

			<div class="col-md-6">
				<div class="progress thin wow fadeInUp animated" data-wow-delay=".4s">
					<div class="progress-bar light-green-bar" role="progressbar" aria-valuenow="87" aria-valuemin="0" data-progress-value="87%" aria-valuemax="100">
						<span class="progress-text">HTML5/CSS3</span> 
						<span class="progress-percent"> 87% </span>
					</div>
				</div>
				<div class="progress thin wow fadeInUp animated" data-wow-delay=".5s">
					<div class="progress-bar light-pink-bar" role="progressbar" aria-valuenow="83" aria-valuemin="0" data-progress-value="83%" aria-valuemax="100">
						<span class="progress-text">WordPress</span> 
						<span class="progress-percent"> 83% </span>
					</div>
				</div>
				<div class="progress thin wow fadeInUp animated" data-wow-delay=".6s">
					<div class="progress-bar light-blue-bar" role="progressbar" aria-valuenow="71" aria-valuemin="0" data-progress-value="71%" aria-valuemax="100" >
						<span class="progress-text">e-Commerce</span> 
						<span class="progress-percent"> 71% </span>
					</div>
				</div>
			</div><!-- /.col-md-6 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.top-skills -->';

  $export['our_skill'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Quality section';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'quality';
  $fe_block_boxes->body = '<div class="quality-section parallax-style">
<div class="pattern">
<div class="container">
<h2 class="text-center plx-section-title">We Value Quality Over Quantity</h2>

<p class="text-center quality-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate officiis corporis, distinctio vitae iusto libero et amet tenetur quae deleniti cum error dolorem eligendi, praesentium reiciendis unde accusamus repellat similique.</p>

<div class="button-container"><button class="btn btn-sm btn-default btn-effect" type="button">Purchase Now</button></div>

<div class="quality-product-img wow bounceInUp"><img alt="Quality Products" src="/sites/default/files/quality.png" /></div>
<!-- /.quality-product-img --></div>
<!-- /.container --></div>
<!-- /.pattern  --></div>
<!-- /.quality-section -->';

  $export['quality'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Quote';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'quote';
  $fe_block_boxes->body = '<div class="quote-section parallax-style">
<div class="pattern">
<div class="container">
<div class="row">
<blockquote>
<p class="quote-description">It’s art if can’t be explained. It’s fashion if no one asks for an explanation. It’s design if it doesn’t need explanation.</p>
<!-- /.quote-description -->

<p class="quote-author">Wouter Stokkel</p>
<!-- /.quote-author --></blockquote>
</div>
<!-- /.row --></div>
<!-- /.container --></div>
<!-- /.pattern --></div>
<!-- /.quote-section -->';

  $export['quote'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Video section';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'video';
  $fe_block_boxes->body = '<div class="video-section parallax-style">
	<div class="pattern">
		<h3 class="video-title">Play Video</h3>
		<p class="play-video"> 
			<a href="http://player.vimeo.com/video/21969942" class="boxer button small play-btn wow rollIn animated" title="Our Video">
				<img class="play-now" src="/sites/default/files/video-play.png" alt="Play Video">
			</a>  
		</p><!-- /.play-video -->
		<h3 class="video-title">Learn More</h3>
	</div><!-- /.pattern -->
</div><!-- /.video-section -->
<div class="video-box" id="video-box">
	
</div><!-- .video-box -->';

  $export['video'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Video background';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'video_background';
  $fe_block_boxes->body = '<!-- Video Background-->
<div id="video-wrapper"></div>
<a id="bgndVideo" class="player" data-property="{videoURL:\'Lxdfg0drTcw\',containment:\'#video-wrapper\',autoPlay:true, mute:true, startAt:0, opacity:1}">My video</a>';

  $export['video_background'] = $fe_block_boxes;

  return $export;
}
