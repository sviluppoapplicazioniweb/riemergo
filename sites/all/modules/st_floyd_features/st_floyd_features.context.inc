<?php
/**
 * @file
 * st_floyd_features.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function st_floyd_features_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_page';
  $context->description = 'Blog page';
  $context->tag = 'blog';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog-page' => 'blog-page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog-block_2' => array(
          'module' => 'views',
          'delta' => 'blog-block_2',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-popular_posts-block' => array(
          'module' => 'views',
          'delta' => 'popular_posts-block',
          'region' => 'sidebar_second',
          'weight' => '-25',
        ),
        'views-blog_tags-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_tags-block_1',
          'region' => 'sidebar_second',
          'weight' => '-24',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog page');
  t('blog');
  $export['blog_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_tag_page';
  $context->description = 'Blog tag page, blog detail';
  $context->tag = 'blog';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog' => 'blog',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'tags' => 'tags',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-popular_posts-block' => array(
          'module' => 'views',
          'delta' => 'popular_posts-block',
          'region' => 'sidebar_second',
          'weight' => '-25',
        ),
        'views-blog_tags-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_tags-block_1',
          'region' => 'sidebar_second',
          'weight' => '-24',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog tag page, blog detail');
  t('blog');
  $export['blog_tag_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home';
  $context->description = 'Home default';
  $context->tag = 'home';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'home' => 'home',
      ),
    ),
  );
  $context->reactions = array(
    'backstretch' => array(
      'source' => array(
        'type' => 'entity',
        'entity' => array(
          'entity_type' => 'node',
          'ids' => '69,70,71',
          'field' => 'field_backstretch_image',
        ),
        'path' => '',
      ),
      'image_style' => '',
      'element' => '',
      'element_other' => '',
      'duration' => '5000',
      'fade' => '0',
      'center_x' => 1,
      'center_y' => 1,
      'random' => 0,
    ),
    'block' => array(
      'blocks' => array(
        'views-intro_slideshow-block' => array(
          'module' => 'views',
          'delta' => 'intro_slideshow-block',
          'region' => 'top',
          'weight' => '-10',
        ),
        'views-about_us-block' => array(
          'module' => 'views',
          'delta' => 'about_us-block',
          'region' => 'content',
          'weight' => '-24',
        ),
        'views-team-block' => array(
          'module' => 'views',
          'delta' => 'team-block',
          'region' => 'content',
          'weight' => '-23',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'content',
          'weight' => '-22',
        ),
        'views-service-block' => array(
          'module' => 'views',
          'delta' => 'service-block',
          'region' => 'content',
          'weight' => '-21',
        ),
        'views-portfolio-block' => array(
          'module' => 'views',
          'delta' => 'portfolio-block',
          'region' => 'content',
          'weight' => '-20',
        ),
        'views-pricing-block' => array(
          'module' => 'views',
          'delta' => 'pricing-block',
          'region' => 'content',
          'weight' => '-19',
        ),
        'views-testimonial-block' => array(
          'module' => 'views',
          'delta' => 'testimonial-block',
          'region' => 'content',
          'weight' => '-18',
        ),
        'views-client-block' => array(
          'module' => 'views',
          'delta' => 'client-block',
          'region' => 'content',
          'weight' => '-17',
        ),
        'block-8' => array(
          'module' => 'block',
          'delta' => '8',
          'region' => 'content',
          'weight' => '-16',
        ),
        'block-6' => array(
          'module' => 'block',
          'delta' => '6',
          'region' => 'content',
          'weight' => '-15',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Home default');
  t('home');
  $export['home'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home_parttern';
  $context->description = 'Home pattern';
  $context->tag = 'home';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'home-pattern' => 'home-pattern',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-intro_slideshow-block_1' => array(
          'module' => 'views',
          'delta' => 'intro_slideshow-block_1',
          'region' => 'top',
          'weight' => '-10',
        ),
        'views-about_us-block' => array(
          'module' => 'views',
          'delta' => 'about_us-block',
          'region' => 'content',
          'weight' => '-24',
        ),
        'views-team-block' => array(
          'module' => 'views',
          'delta' => 'team-block',
          'region' => 'content',
          'weight' => '-23',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'content',
          'weight' => '-22',
        ),
        'views-service-block' => array(
          'module' => 'views',
          'delta' => 'service-block',
          'region' => 'content',
          'weight' => '-21',
        ),
        'views-portfolio-block' => array(
          'module' => 'views',
          'delta' => 'portfolio-block',
          'region' => 'content',
          'weight' => '-20',
        ),
        'views-pricing-block' => array(
          'module' => 'views',
          'delta' => 'pricing-block',
          'region' => 'content',
          'weight' => '-19',
        ),
        'views-testimonial-block' => array(
          'module' => 'views',
          'delta' => 'testimonial-block',
          'region' => 'content',
          'weight' => '-18',
        ),
        'views-client-block' => array(
          'module' => 'views',
          'delta' => 'client-block',
          'region' => 'content',
          'weight' => '-17',
        ),
        'block-8' => array(
          'module' => 'block',
          'delta' => '8',
          'region' => 'content',
          'weight' => '-16',
        ),
        'block-6' => array(
          'module' => 'block',
          'delta' => '6',
          'region' => 'content',
          'weight' => '-15',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Home pattern');
  t('home');
  $export['home_parttern'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home_video';
  $context->description = 'Home video';
  $context->tag = 'home';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'home-video' => 'home-video',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-10' => array(
          'module' => 'block',
          'delta' => '10',
          'region' => 'top',
          'weight' => '-10',
        ),
        'views-intro_slideshow-block_2' => array(
          'module' => 'views',
          'delta' => 'intro_slideshow-block_2',
          'region' => 'top',
          'weight' => '-9',
        ),
        'views-about_us-block' => array(
          'module' => 'views',
          'delta' => 'about_us-block',
          'region' => 'content',
          'weight' => '-24',
        ),
        'views-team-block' => array(
          'module' => 'views',
          'delta' => 'team-block',
          'region' => 'content',
          'weight' => '-23',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'content',
          'weight' => '-22',
        ),
        'views-service-block' => array(
          'module' => 'views',
          'delta' => 'service-block',
          'region' => 'content',
          'weight' => '-21',
        ),
        'views-portfolio-block' => array(
          'module' => 'views',
          'delta' => 'portfolio-block',
          'region' => 'content',
          'weight' => '-20',
        ),
        'views-pricing-block' => array(
          'module' => 'views',
          'delta' => 'pricing-block',
          'region' => 'content',
          'weight' => '-19',
        ),
        'views-testimonial-block' => array(
          'module' => 'views',
          'delta' => 'testimonial-block',
          'region' => 'content',
          'weight' => '-18',
        ),
        'views-client-block' => array(
          'module' => 'views',
          'delta' => 'client-block',
          'region' => 'content',
          'weight' => '-17',
        ),
        'block-8' => array(
          'module' => 'block',
          'delta' => '8',
          'region' => 'content',
          'weight' => '-16',
        ),
        'block-6' => array(
          'module' => 'block',
          'delta' => '6',
          'region' => 'content',
          'weight' => '-15',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Home video');
  t('home');
  $export['home_video'] = $context;

  return $export;
}
